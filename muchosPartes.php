
<?php
include './html/header.html';  //this is everything in the head of the html doc
ini_set('display_errors', true); //debug
include './html/main.html'; //display navbar
/* Make sure we have a session. */
if(!isset($_SESSION)) {
     session_start();
}
require('./php/User.php');
include_once('./php/login.php');

/* Figure out who the logged in user is. */
$user = User::whoami();
print $user;

$db = dbConnection();

if((isset($_GET['index'])) && (is_numeric($_GET['index']))){
	$index = ($_GET['index']);
	switch ($index)
	{
		case 1:include './php/login.php';break;
		case 2:include './php/logout.php';break;
		case 3:include './php/showParts.php';
		include './php/addParts.php';break;
		case 4:include './php/detailPart.php';break;
		case 5:include './php/showParts.php';break;
		case 6:include './php/showWarehouses.php';break;
		case 7:include './php/detailWarehouse.php';break;
		case 8:include './php/admin.php';break;

		default:
	}
}
include './html/footer.html';
?>