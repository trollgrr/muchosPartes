<?php

function dbConnection() {
	// Will remain throughout the lifetime of the script
	static $db = null;
	
	// Force load on first call.
	if ($db === null) {
		require('config.php');
		/* Now that the static variable is set, this object will be returned
		 * on subsequent invocations.
		 */
		$db = new PDO($dsn, $user, $password);
	}
	
	return $db;
}



?>
