
<?php

if (isset($_GET["PartID"]) && is_numeric($_GET["PartID"])) //make sure I have something in there
{
	$PartID = htmlspecialchars($_GET["PartID"]);
		$stmt = $db->prepare("SELECT Name, Description FROM Part WHERE ID = $PartID");
		$stmt->execute();
	if (!$stmt) //display error from mysql - debug
	{
	    echo "\nPDO::errorInfo():\n";
	    print_r($db->errorInfo());
	}else  //no db error
	{ 			//stash the variable values
		$result = $stmt->fetch(PDO::FETCH_OBJ);
		$name = $result->Name;
		$description = $result->Description;
		
	$stmt = $db->prepare("SELECT PartID, WarehouseID, Quantity from StockedPart WHERE PartID = $PartID");
		$stmt->execute();
		if (!$stmt) //display error from mysql - debug
		{
		    echo "\nPDO::errorInfo():\n";
		    print_r($db->errorInfo());
		}else  //no db error
		{

			
//build the table to display warehouse name and stocked quantity
		echo " 
		  <h3 title=\"Nomenclature\">Nombre de Pieza: $name</h3>
		  <h3 title=\"Description\">Descripción: $description</h3>
		  <table class=\"table table-condensed\">
		    <thead>
		      <tr>
		        <th title=\"Warehouse ID\">Número de Almacén</th>
		        <th title=\"Quantity\">Cantidad</th>
		      </tr>
		    </thead>
		    <tbody>
		      <tr>";
//Fill the table with data
			while ($row = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) 
			{
				$data = $row[0] . "</td><td>" . $row[1] . "</td><td>" . $row[2] . "</td><td>";
			    print $data;
				echo "</tr></th>";
			}
			echo "</table>";
			$stmt = null;
		}

		//build the table for the part composition
	$stmt = $db->prepare('SELECT P2.ID AS PartID, P2.Name AS Name, P2.Description AS Description FROM Part P1 JOIN PartComposition PC ON P1.ID = PC.TargetID JOIN Part P2 ON PC.SourceID = P2.ID WHERE P1.ID = $PartID ORDER BY P2.Name');
	$stmt->execute();
//build the table to display part composition
echo "
		  <h3 title=\"Composition\">Composición</h3>
		  <table class=\"table table-condensed\">
		    <thead>
		      <tr>
		      	<th title=\"Part Number\">Numero de Pieza</th>
		        <th title=\"Part Name\">Nombre de Pieza</th>
		        <th title=\"Description\">Descripción</th>
		      </tr>
		    </thead>
		    <tbody>
		      <tr>";
//Fill the table with data
			while ($row = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT))
			{
				$data = $row[0] . "</td><td>" . $row[1] . "</td><td>" . $row[2] . "</td><td>";
			    print $data;
				echo "</tr></th>";
			}
			echo "</table>";
			$stmt = null;

} 	//end if isset
		
	}	//end else for no db error


else{	//stop sending these crazy letters, I've got your number, stalker.
	include './html/404.html';
}



?>