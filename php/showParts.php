
<?php


$query = "SELECT Part.ID, Part.Name, Part.Description, SUM(StockedPart.Quantity) FROM Part, StockedPart WHERE Part.ID = StockedPart.PartID  GROUP BY Part.ID";
$stmt = $db->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL));
$stmt->execute();
echo "<div class=\"container\">
  <h2 title=\"Part Listing\">Listado de Piezas</h2>
  <p>La siguiente información es propietaria y protegidos por el derecho internacional. Visualización de la información está implícito el consentimiento a la política de la empresa.</p>
  <table class=\"table table-condensed\">
    <thead>
          <tr>
        <th title=\"Part Number\">Número de Pieza</th>
        <th title=\"Part Nomenclature\">Nombre</th>
        <th title=\"Description\">Descripción</th>
        <th title=\"Quantity\">Cantidad</th>
      </tr>
    </thead>";

while ($row = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
      $data = $row[0] . "</th><th><a href=\"muchosPartes.php?index=4&PartID=$row[0]\">" . $row[1] . "</th><th>" . $row[2] . "</th><th>" . $row[3] . "</th><th>";
	  echo "<tr><th>";
      print $data;
      echo "</tr></th>";
    }
    echo "</table>";
    $stmt = null;
?>
