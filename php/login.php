
	<h3>Log in</h1>
<?php
	/* Pull session information */
if(!isset($_SESSION)) {
     session_start();
}	
	if (isset($_SESSION['loginError'])) {
		// We could escape this, but it comes from a trusted source.
		echo '<div><b>Error:</b> ' . $_SESSION['loginError'] .
			'</div>';
	
		// The error no longer applies.
		unset($_SESSION['loginError']);
	}
?>	
	<form action='/php/doLogin.php' method='post'>
		<table>
			<tr>
				<th><label for="handle">User Name</label></th>
				<td><input type="text" name="handle"/></td>
			</tr>
			<tr>
				<th><label for="password">Password</label></th>
				<td><input type="password" name="password"/></td>
			</tr>
		</table>
		<input type="submit" value="Log in"/>
	</form>