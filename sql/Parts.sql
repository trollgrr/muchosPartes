drop database if exists CSC435;
create database CSC435;
use CSC435;
-- File: Parts.sql
-- Author: Phillip C.S.R. Kilgore
-- Adapted Bennett B.O.B. Upton
-- Reference implementation for homework assigned on 3/7/2016
DROP PROCEDURE IF EXISTS stock;
DROP PROCEDURE IF EXISTS purchase;
DROP VIEW IF EXISTS StockedPartEx;
DROP TABLE IF EXISTS StockedPart;
DROP TABLE IF EXISTS Warehouse;
DROP TABLE IF EXISTS PartComposition;
DROP TABLE IF EXISTS Part;
DROP TABLE IF EXISTS UserWarehouse;
DROP TABLE IF EXISTS User;
CREATE TABLE User(
	ID INT NOT NULL AUTO_INCREMENT,
	Handle VARCHAR(128) NOT NULL,
	fName VARCHAR(128) NOT NULL,
	lName VARCHAR(128) NOT NULL,
	PwHash VARCHAR(128) NOT NULL,
	PwSalt VARCHAR(128) NOT NULL,
	adminFlag TINYINT(1) DEFAULT 1,
	PRIMARY KEY(ID)
);
CREATE TABLE Part(
	ID INT NOT NULL AUTO_INCREMENT,
	Name VARCHAR(128) NOT NULL,
	Description VARCHAR(255) NOT NULL DEFAULT '',
	PRIMARY KEY(ID)
);
CREATE TABLE PartComposition(
	TargetID INT NOT NULL,
	SourceID INT NOT NULL,
	FOREIGN KEY (TargetID) REFERENCES Part(ID),
	FOREIGN KEY (SourceID) REFERENCES Part(ID),
	PRIMARY KEY(TargetID, SourceID)
);
CREATE TABLE Warehouse(
	ID INT NOT NULL AUTO_INCREMENT,
	Street VARCHAR(255) NOT NULL,
	City VARCHAR(64) NOT NULL,
	State CHAR(2) NOT NULL,
	ZIP CHAR(5) NOT NULL,
	PRIMARY KEY(ID)
);
CREATE TABLE UserWarehouse(
	HouseID INT NOT NULL,
	UserID INT NOT NULL,
	FOREIGN KEY (HouseID) REFERENCES Warehouse(ID),
	FOREIGN KEY (UserID) REFERENCES User(ID),
	PRIMARY KEY(HouseID, UserID)
);
CREATE TABLE StockedPart(
	PartID INT NOT NULL,
	WarehouseID INT NOT NULL,
	Quantity INT DEFAULT 1 NOT NULL,
	FOREIGN KEY(PartID) REFERENCES Part(ID),
	FOREIGN KEY(WarehouseID) REFERENCES Warehouse(ID),
	PRIMARY KEY(PartID, WarehouseID)
);
CREATE VIEW StockedPartEx AS
SELECT P.ID as PartID, P.Name as PartName, W.ID as WarehouseID,
	W.Street as Street, W.City as City, W.State As State, W.ZIP as ZIP,
	Quantity FROM StockedPart AS SP
	LEFT JOIN Part AS P ON SP.PartID=P.ID
	LEFT JOIN Warehouse AS W ON SP.WarehouseID=W.ID
;
-- Dummy data (Users)
INSERT INTO User(Handle, fName, lName, PwHash, PwSalt) VALUES ("froodlover", "Zaphod", "Beeblebrox", "password", "123");
INSERT INTO User(Handle, fName, lName, PwHash, PwSalt) VALUES ("frondlover", "Adam", " ", "password", "123");
INSERT INTO User(Handle, fName, lName, PwHash, PwSalt) VALUES ("foodlover", "Jaime", "Oliver", "password", "123");
INSERT INTO User(Handle, fName, lName, PwHash, PwSalt) VALUES ("frodolover", "Frodo", "Baggins", "password", "123");
-- Dummy data (Part)
INSERT INTO Part(Name, Description) VALUES ("Widget", "It does something!");
INSERT INTO Part(Name, Description) VALUES ("Screw", "Fastens things together");
INSERT INTO Part(Name, Description) VALUES ("Widget Frame", "A frame for a widget");
INSERT INTO Part(Name, Description) VALUES ("Screw driver", "It drives screws");
-- Dummy data (Part Composition)
INSERT INTO PartComposition(TargetID, SourceID) VALUES (1, 2);
INSERT INTO PartComposition(TargetID, SourceID) VALUES (1, 3);
-- Dummy data (Warehouse)
INSERT INTO Warehouse (Street, City, State, ZIP) VALUES ("Apostolic Palace ", "Vatican City", "VC", "00120");
INSERT INTO Warehouse (Street, City, State, ZIP) VALUES ("1060 West Addison", "Chicago", "IL", "60613");
INSERT INTO Warehouse (Street, City, State, ZIP) VALUES ("212B Baker St.", "London", "TX", "76854");
INSERT INTO Warehouse (Street, City, State, ZIP) VALUES ("One University Place", "Shreveport", "LA", "71115");
-- Dummy data (Users/Warehouse Association)
INSERT INTO UserWarehouse(HouseID, UserID) VALUES (1, 2);
INSERT INTO UserWarehouse(HouseID, UserID) VALUES (2, 1);
INSERT INTO UserWarehouse(HouseID, UserID) VALUES (3, 4);
INSERT INTO UserWarehouse(HouseID, UserID) VALUES (4, 3);
-- Dummy data (StockedPart)
INSERT INTO StockedPart(PartID, WarehouseID) VALUES (1, 1);
INSERT INTO StockedPart(PartID, WarehouseID, Quantity) VALUES (1, 2, 1000);
INSERT INTO StockedPart(PartID, WarehouseID, Quantity) VALUES (2, 2, 9001);
INSERT INTO StockedPart(PartID, WarehouseID, Quantity) VALUES (3, 2, 500);
DELIMITER //
-- Procedure: stock
--
-- Add stock of a certain item to a warehouse. If it is not stocked, a 
-- stock entry is made for it.
--
-- Parameters:
--  part - The ID of the part to stock (it must exist in Part).
--  warehouse - The ID of the warehouse to stock (it must exist in Warehouse).
--  quantity - The amount of the object to stock the warehouse with.
CREATE PROCEDURE stock(part int, warehouse int, amount int) BEGIN DECLARE current int;
START TRANSACTION;
-- Determine our current amount
SELECT Quantity INTO current FROM StockedPart WHERE PartID=part AND WarehouseID=warehouse;
-- No such part stocked here. Create the record.
IF current IS NULL THEN INSERT INTO StockedPart(PartID, WarehouseID, Quantity) VALUES (part, warehouse, amount);
-- Update the quantity by adding the amount.
ELSE SET current = current + amount;
UPDATE StockedPart SET Quantity=current WHERE PartID=part AND WarehouseID=warehouse;
END IF;
COMMIT;
END//
-- Procedure: purchase
--
-- Remove stock of a certain item to a warehouse. If it is not stocked, a 
-- stock entry is made for it.
--
-- Parameters:
--  part - The ID of the part to stock (it must exist in Part).
--  warehouse - The ID of the warehouse to stock (it must exist in Warehouse).
--  quantity - The amount of the object to stock the warehouse with.
--
-- Returns: The number of items removed from the warehouse on success;
-- otherwise, the number of items needed before the purchase can take
-- place.
CREATE PROCEDURE purchase(part int, warehouse int, amount int) BEGIN DECLARE current INT;
START TRANSACTION;
-- Determine our current amount
SELECT Quantity INTO current FROM StockedPart WHERE PartID=part AND WarehouseID=warehouse;
-- No such part stocked here.
IF current IS NULL THEN SET current = 0 - amount;
ROLLBACK;
-- Insufficient stock.
ELSEIF current < amount THEN SET current = current - amount;
ROLLBACK;
ELSE SET current = current - amount;
UPDATE StockedPart SET Quantity=current WHERE PartID=part AND WarehouseID=warehouse;
COMMIT;
END IF;
	SELECT current;
END//
DELIMITER ;
